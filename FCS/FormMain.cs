﻿using FlyCoordinationSystem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FCS
{
    public partial class FormMain : Form
    {
        BindingList<Samolot> listaSamolotow = new BindingList<Samolot>();
        public FormMain()
        {
            InitializeComponent();
            listaSamolotow.AllowNew = true;
            listaSamolotow.AllowEdit = true;
            listaSamolotow.AllowRemove = true;
          
            gridControlSamoloty.DataSource = listaSamolotow;
            gridViewSamoloty.Columns["grafika"].Visible = false;
            repositoryItemComboboxKierunekWiatru.Items.AddRange(new object[] { Kierunek.East, Kierunek.West, Kierunek.South, Kierunek.SouthEast, Kierunek.SouthWest, Kierunek.North, Kierunek.NorthEast, Kierunek.NorthWest });
        }
        
        private void gridViewSamoloty_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            richTextBoxLogi.Text += "\n " + DateTime.Now + " : Dodano nowy samolot";
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
           
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
        }

        private void gridViewSamoloty_RowDeleting(object sender, DevExpress.Data.RowDeletingEventArgs e)
        {
            richTextBoxLogi.Text += "\n " + DateTime.Now + " : Usunięto samolot o nazwie: "+((Samolot)e.Row).nazwa;
        }
    }
}
